package com.vulcanization.proj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Wizyty {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private java.sql.Date data;
  private String godzina;
  private Long zaklady_wulkanizacyjne_id;
  private Long zamowienia_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public java.sql.Date getData() {
    return data;
  }

  public void setData(java.sql.Date data) {
    this.data = data;
  }

  public String getGodzina() {
    return godzina;
  }

  public void setGodzina(String godzina) {
    this.godzina = godzina;
  }

  public Long getZaklady_wulkanizacyjne_id() {
    return zaklady_wulkanizacyjne_id;
  }

  public void setZaklady_wulkanizacyjne_id(Long zaklady_wulkanizacyjne_id) {
    this.zaklady_wulkanizacyjne_id = zaklady_wulkanizacyjne_id;
  }

  public Long getZamowienia_id() {
    return zamowienia_id;
  }

  public void setZamowienia_id(Long zamowienia_id) {
    this.zamowienia_id = zamowienia_id;
  }
}
