package com.vulcanization.proj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dostawcy {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String nazwa;
  private String nip;
  private String regon;
  private Long adresy_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNazwa() {
    return nazwa;
  }

  public void setNazwa(String nazwa) {
    this.nazwa = nazwa;
  }

  public String getNip() {
    return nip;
  }

  public void setNip(String nip) {
    this.nip = nip;
  }

  public String getRegon() {
    return regon;
  }

  public void setRegon(String regon) {
    this.regon = regon;
  }

  public Long getAdresy_id() {
    return adresy_id;
  }

  public void setAdresy_id(Long adresy_id) {
    this.adresy_id = adresy_id;
  }
}
