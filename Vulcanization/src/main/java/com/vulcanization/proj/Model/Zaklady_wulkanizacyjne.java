package com.vulcanization.proj.Model;

import javax.persistence.*;

@Entity
public class Zaklady_wulkanizacyjne {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String nazwa;
  private Long nr_tel;
  private Long adresy_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNazwa() {
    return nazwa;
  }

  public void setNazwa(String nazwa) {
    this.nazwa = nazwa;
  }

  public Long getNr_tel() {
    return nr_tel;
  }

  public void setNr_tel(Long nr_tel) {
    this.nr_tel = nr_tel;
  }

  public Long getAdresy_id() {
    return adresy_id;
  }

  public void setAdresy_id(Long adresy_id) {
    this.adresy_id = adresy_id;
  }
}
