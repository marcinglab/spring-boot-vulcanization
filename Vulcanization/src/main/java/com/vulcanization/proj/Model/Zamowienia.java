package com.vulcanization.proj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Zamowienia {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private java.sql.Timestamp data_zam;
  private java.sql.Date data_dos;
  private Long ilosc;
  private Double cena_zam;
  private Long opony_id;
  private Long klienci_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public java.sql.Timestamp getData_zam() {
    return data_zam;
  }

  public void setData_zam(java.sql.Timestamp data_zam) {
    this.data_zam = data_zam;
  }

  public java.sql.Date getData_dos() {
    return data_dos;
  }

  public void setData_dos(java.sql.Date data_dos) {
    this.data_dos = data_dos;
  }

  public Long getIlosc() {
    return ilosc;
  }

  public void setIlosc(Long ilosc) {
    this.ilosc = ilosc;
  }

  public Double getCena_zam() {
    return cena_zam;
  }

  public void setCena_zam(Double cena_zam) {
    this.cena_zam = cena_zam;
  }

  public Long getOpony_id() {
    return opony_id;
  }

  public void setOpony_id(Long opony_id) {
    this.opony_id = opony_id;
  }

  public Long getKlienci_id() {
    return klienci_id;
  }

  public void setKlienci_id(Long klienci_id) {
    this.klienci_id = klienci_id;
  }
}
