package com.vulcanization.proj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Opony {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private Long szerokosc;
  private Long wysokosc;
  private Long srednica;
  private String budowa;
  private String kierunkowosc;
  private String sezon;
  private Double cena;
  private Long dostawcy_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getSzerokosc() {
    return szerokosc;
  }

  public void setSzerokosc(Long szerokosc) {
    this.szerokosc = szerokosc;
  }

  public Long getWysokosc() {
    return wysokosc;
  }

  public void setWysokosc(Long wysokosc) {
    this.wysokosc = wysokosc;
  }

  public Long getSrednica() {
    return srednica;
  }

  public void setSrednica(Long srednica) {
    this.srednica = srednica;
  }

  public String getBudowa() {
    return budowa;
  }

  public void setBudowa(String budowa) {
    this.budowa = budowa;
  }

  public String getKierunkowosc() {
    return kierunkowosc;
  }

  public void setKierunkowosc(String kierunkowosc) {
    this.kierunkowosc = kierunkowosc;
  }

  public String getSezon() {
    return sezon;
  }

  public void setSezon(String sezon) {
    this.sezon = sezon;
  }

  public Double getCena() {
    return cena;
  }

  public void setCena(Double cena) {
    this.cena = cena;
  }

  public Long getDostawcy_id() {
    return dostawcy_id;
  }

  public void setDostawcy_id(Long dostawcy_id) {
    this.dostawcy_id = dostawcy_id;
  }
}
