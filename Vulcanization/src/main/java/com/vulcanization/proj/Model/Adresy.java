package com.vulcanization.proj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Adresy {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String miasto;
  private String ulica;
  private String nr_budynku;
  private String nr_lokalu;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMiasto() {
    return miasto;
  }

  public void setMiasto(String miasto) {
    this.miasto = miasto;
  }

  public String getUlica() {
    return ulica;
  }

  public void setUlica(String ulica) {
    this.ulica = ulica;
  }

  public String getNr_budynku() {
    return nr_budynku;
  }

  public void setNr_budynku(String nr_budynku) {
    this.nr_budynku = nr_budynku;
  }

  public String getNr_lokalu() {
    return nr_lokalu;
  }

  public void setNr_lokalu(String nr_lokalu) {
    this.nr_lokalu = nr_lokalu;
  }
}
