package com.vulcanization.proj.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vulcanization.proj.Model.Klienci;

//import com.vulcanization.proj.Model.Klienci;

@Controller
public class homeController {

    @RequestMapping(value = {"/", "/home", "/index"}, method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/testTemplate")
    public String testSite() {
        return "testTemplate";
    }

    @RequestMapping(value = "/users/login")
    public String userLogin(Model model, Klienci klienci) {
    	model.addAttribute("login", klienci.getLogin());
		model.addAttribute("haslo", klienci.getHaslo());
    	return "users/login";
    }

    @RequestMapping(value = "/users/register")
    public String userRegister() {
        return "users/register";
    }

    @RequestMapping(value = "/users/index")
    public String userIndex(@ModelAttribute Klienci klienci){
        return "users/index";
    }

/*
    @RequestMapping(value = "/testTemplate")
    public String testSite() {
        return "testTemplate";
    }

    @RequestMapping(value = "/testTemplate")
    public String testSite() {
        return "testTemplate";
    }
*/

}

