package com.vulcanization.proj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VulcanizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(VulcanizationApplication.class, args);
	}
}
